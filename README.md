# Python Wrapper for Investec Programmable Banking Open API

This is my first attempt at creating a client in Python.

## Investec Client
The [client class](investec/client.py) is very simple at this point and only contains functions that wrap the API endpoints.
There is no management of state for the Bearer token yet so it doesn't take the token time to live into account.
All responses are returned from the client in the form of dictionaries.

## Getting started
Create a virtual environment:

On macOS and Linux: `python3 -m venv .venv`\
On Windows: `py -m venv .venv`

Then install the requirements:

`.venv/bin/python -m pip install -r requirements.txt`

## Usage
Both clients implement the same interface so the usage is similar

```Python
    # Create instance of the client, get a list of the accounts and print the size of the list
    from investec.client import OpenAPIClient

    client_id = "YOUR CLIENT ID"
    secret = "YOUR SECRET"

    client = OpenAPIClient(client_id, secret)

    accounts = client.get_accounts()
    balance = client.get_account_balance(accounts[0]["accountId"])
    transactions = client.get_account_transactions(accounts[0]["accountId"])

    print("accounts = %d" % len(accounts))
    print("balance = %d" % balance["availableBalance"])
    print("transactions = %d" % len(transactions))
```

## Examples

For an examples take a look at [app.py](app.py)
