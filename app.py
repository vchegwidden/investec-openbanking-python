from investec.client import OpenAPIClient

client_id = "YOUR CLIENT ID"
secret = "YOUR SECRET"

client = OpenAPIClient(client_id, secret)

accounts = client.get_accounts()
balance = client.get_account_balance(accounts[0]["accountId"])
transactions = client.get_account_transactions(accounts[0]["accountId"])

print("accounts = %d" % len(accounts))
print("balance = %d" % balance["availableBalance"])
print("transactions = %d" % len(transactions))