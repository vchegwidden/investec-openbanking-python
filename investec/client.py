# -*- coding: utf-8 -*-

"""
investec.client
~~~~~~~~~~~~~~
This module contains Client class responsible for communicating with
the Investec API.
"""

import requests
import datetime
import hashlib

class HTTPBearerAuth(requests.auth.AuthBase):
    """Helper class to add Bearer Auth to requests"""
    def __init__(self, token: str):
        """
        Initialise with the token

        :param token: The bearer token
        """
        self.token = token

    def __call__(self, r: requests.Request) -> requests.Request:
        """
        Sets the correct header for the request

        :param r: The request
        :returns: The modified request
        """
        r.headers["Authorization"] = "Bearer " + self.token
        return r

class OpenAPIClient(object):
    """
    An instance of this class communicates with the Investec API.

    >>> client = investec.Client("Your Client ID", "Your Secret")
    """

    def __init__(self, client_id: str, secret: str, timeout: int=30):
        """
        Create a client instance with the provided options.

        :param client_id: Client ID obtained from the Investec Site
        :param secret: Secret obtained from the Investec Site
        :param timeout: The timeout for API requests in seconds
        """

        if not client_id or not secret:
            raise ValueError("Both the client_id and secret are required.")
        self.client_id = client_id
        self.secret = secret
        self.timeout = timeout
        self.requests_session = requests.Session()
        self.api_host = "https://openapi.investec.com"
        self.token_expires = datetime.datetime.now()
        self.token = None

    def _call_api(self, service_url: str, method: str="get", params: dict=None, body: str=None) -> dict:
        """
        Helper function to call the API

        :param service_url: The URL to the service
        :param method: The HTTP verb
        :param params: The query string parameters for the request
        :param body: The payload for the request
        :returns: The API response
        :raises HTTPError: In the event the response is not valid
        """

        auth = None
        if service_url.endswith("/token"):
            auth = requests.auth.HTTPBasicAuth(self.client_id, self.secret)
        else:
            if not self.token or datetime.datetime.now() >= self.token_expires:
                self._get_access_token() # Need to get a new token
            auth = HTTPBearerAuth(self.token)
        request = getattr(self.requests_session, method)
        headers = {"Accept": "application/json"}
        if method == "post":
            headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8"
        response = request("%s/%s" % (self.api_host, service_url), params=params, data=body, headers=headers, auth=auth, timeout=self.timeout)
        try:
            response.raise_for_status()
            content = response.json()
            if "data" in content:
                return content["data"]
            return content
        except:
            raise requests.exceptions.HTTPError(response.status_code, response.content)

    def _get_access_token(self) -> None:
        """Get an access token"""
        response = self._call_api("identity/v2/oauth2/token", method="post", body="grant_type=client_credentials&scope=accounts")
        self.token = response["access_token"]
        token_expiry = response["expires_in"] - (5 * 60) # Expire the token 5 minutes before it actually expires
        self.token_expires = datetime.datetime.now() + datetime.timedelta(seconds=token_expiry)

    def get_accounts(self) -> dict:
        """
        Gets the available accounts

        :returns: The accounts
        """
        return self._call_api("za/pb/v1/accounts")["accounts"]

    def get_account_transactions(self, account_id: str, from_date: datetime=None, to_date: datetime=None) -> dict:
        """
        Gets the transactions for an account and generates a hash for each transaction

        :param account_id: The account to get the transactions for
        :param from_date: Only get transactions from this date (inclusive)
        :param to_date: Only get transactions to this date (inclusive)
        :returns: The transactions
        """
        if from_date and to_date and to_date < from_date:
            raise ValueError("The from_date must be before the to_date")
        params = dict()
        if from_date:
            params["fromDate"] = from_date.strftime("%Y-%m-%d")
        if to_date:
            params["toDate"] = to_date.strftime("%Y-%m-%d")
        transactions = self._call_api("za/pb/v1/accounts/%s/transactions" % account_id, params=params)["transactions"]
        # Add a hash to each transaction
        processed_digests = list()
        for t in transactions:
            transaction_hash = hashlib.sha256()
            transaction_hash.update(t["postingDate"].encode("utf-8"))
            transaction_hash.update(t["description"].encode("utf-8"))
            transaction_hash.update(str(t["amount"]).encode("utf-8"))
            hex_digest = transaction_hash.hexdigest()
            while hex_digest in processed_digests:
                transaction_hash.update("a".encode("utf-8"))
                hex_digest = transaction_hash.hexdigest()
            processed_digests.append(hex_digest)
            t["transactionHash"] = hex_digest
        return transactions

    def get_account_balance(self, account_id: str) -> dict:
        """
        Gets the balance for an account

        :param account_id: The account to get the balance for
        :returns: The balance information
        """
        return self._call_api("za/pb/v1/accounts/%s/balance" % account_id)
